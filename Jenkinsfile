pipeline {
    agent any
    options {
        gitLabConnection('React-demo-app-project-gitlab')
        gitlabBuilds(builds: ['Clone', 'Sonar Scan', 'Quality Gate', 'Unit Test', 'Build','Docker Package', 'Deploy Testing','Leader Approved','Deploy Staging'])
    }
    stages {
        stage('Clone') {
            steps {
                updateGitlabCommitStatus name: 'Clone', state: 'pending'
                git branch: 'main', url: 'https://gitlab.com/hoanggia24121998/react-demo-app.git'
                updateGitlabCommitStatus name: 'Clone', state: 'success'
            }
        }
        stage('Sonar Scan & Analysis') {
            steps{
                updateGitlabCommitStatus name: 'Sonar Scan', state: 'pending'
                nodejs(nodeJSInstallationName: 'nodejs'){
                  withSonarQubeEnv(installationName: 'sq1'){
                      sh 'npm install sonar-scanner'
                      sh 'npm run sonar'
                  }
                }
            }
               post {
                success {
                    updateGitlabCommitStatus name: 'Sonar Scan', state: 'success'
                }
                failure {
                    updateGitlabCommitStatus name: 'Sonar Scan', state: 'failed'
                }
               }
        }
        stage('Quality Gate'){
            steps {
                timeout(time: 2, unit: 'MINUTES'){
                    waitForQualityGate abortPipeline: true
                }
            }
            post {
                success {
                    updateGitlabCommitStatus name: 'Quality Gate', state: 'success'
                }
                failure {
                    updateGitlabCommitStatus name: 'Quality Gate', state: 'failed'
                }
            }
        }
        stage('Unit Test'){
            steps {
                updateGitlabCommitStatus name: 'Unit Test', state: 'pending'
                nodejs(nodeJSInstallationName: 'nodejs'){
                    sh 'npm run test'
                }
            }
            post {
                success {
                    publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir: 'coverage/lcov-report',  reportFiles: 'index.html', reportName: 'Jest Report', reportTitles: '', useWrapperFileDirectly: true])
                    updateGitlabCommitStatus name: 'Unit Test', state: 'success'
                }
                 failure {
                    updateGitlabCommitStatus name: 'Unit Test', state: 'failed'
                }
            }
        }
        stage('Build'){
            steps {
                updateGitlabCommitStatus name: 'Build', state: 'pending'
                nodejs(nodeJSInstallationName: 'nodejs'){
                    sh 'npm install'
                }
            }
            post {
                success {
                    updateGitlabCommitStatus name: 'Build', state: 'success'
                }
                failure {
                    updateGitlabCommitStatus name: 'Build', state: 'failed'
                }
            }
        }
        stage('Docker Package Image') {
            steps{
                withDockerRegistry(credentialsId: 'Docker-hub', url: 'https://index.docker.io/v1/') {    
                    echo 'Starting build image'
                    updateGitlabCommitStatus name: 'Docker Package', state: 'pending'
                    sh 'docker build -t hoanggia2412/reactjs-app-jenkins:$(git rev-parse --short HEAD) .'
                    sh 'docker push hoanggia2412/reactjs-app-jenkins:$(git rev-parse --short HEAD)' 
                }
            }
             post {
                success {
                    updateGitlabCommitStatus name: 'Docker Package', state: 'success'
                }
                failure {
                    updateGitlabCommitStatus name: 'Docker Package', state: 'failed'
                }
               }
        }
        stage('Deploy Testing'){
            steps{
                updateGitlabCommitStatus name: 'Deploy Testing', state: 'pending'
                echo "Start deploy Testing"
                sh 'sudo docker stop react-app'
                sh 'sudo docker rm react-app'
                sh 'sudo docker run -d -p 9000:80 --name react-app hoanggia2412/reactjs-app-jenkins:$(git rev-parse --short HEAD)'
            }
             post {
                success {
                    updateGitlabCommitStatus name: 'Deploy Testing', state: 'success'
                }
                failure {
                    updateGitlabCommitStatus name: 'Deploy Testing', state: 'failed'
                }
               }
        }
        stage('Leader Approved'){
            options {
                timeout(time: 30, unit: 'MINUTES')
            }
            steps{
                updateGitlabCommitStatus name: 'Leader Approved', state: 'pending'
                input "Please approve to process with deployment"
                
            }
            post {
                    success {
                        updateGitlabCommitStatus name: 'Leader Approved', state: 'success'
                    }
                    failure {
                        updateGitlabCommitStatus name: 'Leader Approved', state: 'failed'
                    }
               }
        }
        stage('Deploy Staging') {
            steps{
                updateGitlabCommitStatus name: 'Deploy Staging', state: 'pending'
                echo "Start deploy"
                sh 'ansible-playbook --extra-vars DOCKER_TAG=$(git rev-parse --short HEAD) stg-playbook.yml '
            }
            post {
                success {
                        updateGitlabCommitStatus name: 'Deploy Staging', state: 'success'
                    }
                failure {
                        updateGitlabCommitStatus name: 'Deploy Staging', state: 'failed'
                    }
               }
        }
        
    }
}
